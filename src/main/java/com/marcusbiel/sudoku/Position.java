package com.marcusbiel.sudoku;

import static com.marcusbiel.sudoku.SudokuConstants.BOX_LENGTH;
import static com.marcusbiel.sudoku.SudokuConstants.UNIT_LENGTH;

/* Immutable class, thread safe */
public final class Position {

    private final int cellIndex;
    private final int rowIndex;
    private final int columnIndex;
    private final int boxIndex;

    public static Position of(int cellIndex) {
        return new Position(cellIndex);
    }

    private Position(int cellIndex) {
        this.cellIndex = cellIndex;
        this.rowIndex = cellIndex / UNIT_LENGTH;
        this.columnIndex = cellIndex % UNIT_LENGTH;
        this.boxIndex = (rowIndex / BOX_LENGTH) * BOX_LENGTH + this.columnIndex / BOX_LENGTH;
    }

    public int cellIndex() {
        return cellIndex;
    }

    public int rowIndex() {
        return rowIndex;
    }

    public int columnIndex() {
        return columnIndex;
    }

    public int boxIndex() {
        return boxIndex;
    }

}
