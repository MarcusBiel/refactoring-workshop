package com.marcusbiel.sudoku;

class SudokuConstants {
    public static final int NO_SOLUTION_FOUND = 0;
    public static final int TWO_SOLUTIONS_FOUND = 2;
    public static final int ONE_SOLUTION_FOUND = 1;
    public static final int VALUE_1 = 1;
    public static final int VALUE_9 = 9;
    public static final char NEW_LINE_CHAR = '\n';
    public static final char EMPTY_DOT_CHAR = '.';
    public static final char EMPTY_ZERO_CHAR = '0';
    public static final char VALUE_1_CHAR = '1';
    public static final char VALUE_9_CHAR = '9';
    public static final int MAX_CELL_COUNT = 81;
    public static final int UNIT_LENGTH = 9;
    public static final int BOX_LENGTH = 3;
    public static final int EMPTY_VALUE = 0;
    public static final int NO_EMPTY_CELL_FOUND = -1;

    public static final String SPACE_CHAR = " ";
    public static final String EMPTY_VALUE_PRINT_CHAR = ".";
    public static final String VERTICAL_BORDER_CHAR = "|";
    public static final String HORIZONTAL_BORDER_CHAR = "-------------------------";
    public static final String ENCODING = "UTF-8";

    private SudokuConstants() {
        throw new IllegalStateException("do not invoke");
    }
}
