package com.marcusbiel.sudoku;

import java.util.BitSet;

import static com.marcusbiel.sudoku.SudokuConstants.*;

final class SudokuFactory {

    public static Grid create(String filePath) {
        Cell[] sudokuCells = initSudokuCells(filePath);
        return toGrid(sudokuCells);
    }

    private static BitSet[] initBitSet() {
        BitSet[] bitSets = new BitSet[UNIT_LENGTH];
        for (int i = 0; i < bitSets.length; i++) {
            bitSets[i] = new BitSet(UNIT_LENGTH);
        }
        return bitSets;
    }

    private static Grid toGrid(Cell[] sudokuCells) {
        BitSet[] rowsBitSet = initBitSet();
        BitSet[] columnsBitSet = initBitSet();
        BitSet[] boxBitSet = initBitSet();

        for (int cellIndex = 0; cellIndex < sudokuCells.length; cellIndex++) {
            int value = sudokuCells[cellIndex].value();
            rowsBitSet[rowIndex(cellIndex)].set(value);
            columnsBitSet[columnIndex(cellIndex)].set(value);
            boxBitSet[boxIndex(cellIndex)].set(value);
        }
        return new Grid(sudokuCells, rowsBitSet, columnsBitSet, boxBitSet);
    }

    private static Cell[] initSudokuCells(String filePath) {
        int charIndex = 0;
        int cellIndex = 0;

        char[] sudokuCharacters = FileParser.parse(filePath);
        Cell[] sudokuCells = new Cell[MAX_CELL_COUNT];

        while (cellIndex < MAX_CELL_COUNT) {
            int character = sudokuCharacters[charIndex++];
            if (isFixedValue(character) || isEmptyCell(character)) {
                sudokuCells[cellIndex] = Cell.of(cellIndex, toIntValue(character));
                cellIndex++;
            }
        }
        return sudokuCells;
    }

    private static boolean isFixedValue(int character) {
        return character >= VALUE_1_CHAR && character <= VALUE_9_CHAR;
    }

    private static boolean isEmptyCell(int character) {
        return character == EMPTY_DOT_CHAR || character == EMPTY_ZERO_CHAR;
    }

    private static int toIntValue(int character) {
        if(isEmptyCell(character)){
            return EMPTY_VALUE;
        }
        return Character.getNumericValue(character);
    }

    private static int rowIndex(int cellIndex) {
        return cellIndex / UNIT_LENGTH;
    }

    private static int boxIndex(int cellIndex) {
        return (rowIndex(cellIndex) / BOX_LENGTH) * BOX_LENGTH + columnIndex(cellIndex) / BOX_LENGTH;
    }

    private static int columnIndex(int cellIndex) {
        return cellIndex % UNIT_LENGTH;
    }

}
