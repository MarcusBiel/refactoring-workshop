package com.marcusbiel.sudoku;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.marcusbiel.sudoku.SudokuConstants.ENCODING;

final class FileParser {

    public static char[] parse(String filePath) {
        try {
            byte[] byteArray = Files.readAllBytes(Paths.get(filePath));
            return new String(byteArray, ENCODING).toCharArray();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}


