package com.marcusbiel.sudoku;

import java.util.ArrayList;
import java.util.Collection;

import static com.marcusbiel.sudoku.Printer.printResult;

class SudokuRunner {

    public static void main(String[] args) {
        run(args[0]);
    }

    public static Collection<String> run(String filePath) {
        Collection<String> solutions = new ArrayList<>();
        Grid grid = SudokuFactory.create(filePath);
        SudokuSolver.solve(grid, solutions);
        printResult(grid, solutions);
        return solutions;
    }
}
