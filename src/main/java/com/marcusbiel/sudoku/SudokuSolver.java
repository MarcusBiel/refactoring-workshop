package com.marcusbiel.sudoku;

import java.util.Collection;

import static com.marcusbiel.sudoku.SudokuConstants.*;

final class SudokuSolver {

    public static void solve(Grid grid, Collection<String> solutions) {
        if (solutions.size() >= TWO_SOLUTIONS_FOUND) {
            return;
        }

        int cell = grid.findEmptyCell();

        if (cell == NO_EMPTY_CELL_FOUND) {
            solutions.add(grid.toString());
            return;
        }

        for (int value = VALUE_1; value <= VALUE_9; value++) {
            if (grid.isAllowed(cell, value)) {
                grid.update(cell, value);
                solve(grid, solutions);
                grid.reset(cell);
            }
        }
    }
}
