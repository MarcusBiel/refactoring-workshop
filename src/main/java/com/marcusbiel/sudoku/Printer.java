package com.marcusbiel.sudoku;

import java.util.Collection;

import static com.marcusbiel.sudoku.SudokuConstants.*;

final class Printer {

    public static void printResult(Grid grid, Collection<String> solutions) {
        System.out.println("Original");
        System.out.println(asString(grid.cells()));

        if (solutions.size() == NO_SOLUTION_FOUND) {
            System.out.println("Unsolvable");
        } else if (solutions.size() == ONE_SOLUTION_FOUND) {
            System.out.println("Solved");
        } else {
            System.out.println("At least two solutions");
        }

        for (String solution : solutions) {
            System.out.println(solution);
        }
        System.out.println();
        System.out.println();
    }

    public static String asString(int[] gridCells) {
        StringBuilder builder = new StringBuilder();
        for (int rowIndex = 0; rowIndex < UNIT_LENGTH; rowIndex++) {
            if (rowIndex % BOX_LENGTH == 0) {
                builder.append(HORIZONTAL_BORDER_CHAR);
                builder.append(NEW_LINE_CHAR);
            }
            for (int columnIndex = 0; columnIndex < UNIT_LENGTH; columnIndex++) {
                if (columnIndex % BOX_LENGTH == 0) {
                    builder.append(VERTICAL_BORDER_CHAR);
                    builder.append(SPACE_CHAR);
                }
                int value = gridCells[rowIndex * UNIT_LENGTH + columnIndex];
                if (isEmpty(value)) {
                    builder.append(EMPTY_VALUE_PRINT_CHAR);
                    builder.append(SPACE_CHAR);
                } else {
                    builder.append(value);
                    builder.append(SPACE_CHAR);
                }
            }
            builder.append(VERTICAL_BORDER_CHAR);
            builder.append(NEW_LINE_CHAR);
        }
        builder.append(HORIZONTAL_BORDER_CHAR);
        return builder.toString();
    }

    private static boolean isEmpty(int value) {
        return value == EMPTY_VALUE;
    }

    private Printer() {
        throw new IllegalStateException("do not invoke");
    }
}


