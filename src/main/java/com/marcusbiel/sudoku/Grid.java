package com.marcusbiel.sudoku;

import java.util.BitSet;

import static com.marcusbiel.sudoku.SudokuConstants.*;

public class Grid {

    private final int[] cells;

    private final BitSet[] rowsBitSet;
    private final BitSet[] columnsBitSet;
    private final BitSet[] boxBitSet;

    public Grid(Cell[] cells, BitSet[] rowsBitSet, BitSet[] columnsBitSet, BitSet[] boxBitSet) {
        this.cells = cells;
        this.rowsBitSet = rowsBitSet;
        this.columnsBitSet = columnsBitSet;
        this.boxBitSet = boxBitSet;
    }

    public int findEmptyCell() {
        for (int cellIndex: cells) {
            if (isEmpty(cells[cellIndex])) {
                return cellIndex;
            }
        }
        return NO_EMPTY_CELL_FOUND;
    }

    private boolean isEmpty(int cell) {
        return cell == EMPTY_VALUE;
    }

    public boolean isAllowed(int cellIndex, int value) {
        return (!rowsBitSet[rowIndex(cellIndex)].get(value))
                && (!columnsBitSet[columnIndex(cellIndex)].get(value))
                && (!boxBitSet[boxIndex(cellIndex)].get(value));
    }

    public void update(int cellIndex, int value) {
        cells[cellIndex] = value;
        rowsBitSet[rowIndex(cellIndex)].set(value);
        columnsBitSet[columnIndex(cellIndex)].set(value);
        boxBitSet[boxIndex(cellIndex)].set(value);
    }

    public void reset(int cellIndex) {
        int value = cells[cellIndex];

        rowsBitSet[rowIndex(cellIndex)].clear(value);
        columnsBitSet[columnIndex(cellIndex)].clear(value);
        boxBitSet[boxIndex(cellIndex)].clear(value);

        cells[cellIndex] = EMPTY_VALUE;
    }

    private int rowIndex(int cellIndex) {
        return cellIndex / UNIT_LENGTH;
    }

    private int boxIndex(int cellIndex) {
        return (rowIndex(cellIndex) / BOX_LENGTH) * BOX_LENGTH + columnIndex(cellIndex) / BOX_LENGTH;
    }

    private int columnIndex(int cellIndex) {
        return cellIndex % UNIT_LENGTH;
    }

    public int[] cells() {
        return cells;
    }

    public String toString() {
        return Printer.asString(this.cells);
    }
}