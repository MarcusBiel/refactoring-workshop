package com.marcusbiel.sudoku;

import static com.marcusbiel.sudoku.SudokuConstants.EMPTY_VALUE;

public final class Cell {

    private final Position position;
    private int value;

    public static Cell of(int cellIndex, int value) {
        return new Cell(cellIndex, value);
    }

    private Cell(int cellIndex, int value) {
        this.position = Position.of(cellIndex);
        this.value = value;
    }

    public int value() {
        return value;
    }

    public int cellIndex() {
        return position.cellIndex();
    }

    public int rowIndex() {
        return position.rowIndex();
    }

    public int columnIndex() {
        return position.columnIndex();
    }

    public int boxIndex() {
        return position.boxIndex();
    }

}
