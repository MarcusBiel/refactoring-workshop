package com.marcusbiel.sudoku;

import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Iterator;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class SudokuRunnerTest {

    private static final String FILE_PATH = "/Users/mbiel/workspace/live-refactoring/src/main/resources/Grid.txt";

    private static final String FIRST_SOLUTION = "-------------------------\n" +
            "| 1 5 3 | 9 2 8 | 7 4 6 |\n" +
            "| 4 6 8 | 1 5 7 | 2 3 9 |\n" +
            "| 2 7 9 | 4 6 3 | 1 5 8 |\n" +
            "-------------------------\n" +
            "| 3 1 6 | 2 7 4 | 8 9 5 |\n" +
            "| 8 2 4 | 5 1 9 | 3 6 7 |\n" +
            "| 7 9 5 | 3 8 6 | 4 1 2 |\n" +
            "-------------------------\n" +
            "| 5 3 1 | 7 9 2 | 6 8 4 |\n" +
            "| 9 8 2 | 6 4 1 | 5 7 3 |\n" +
            "| 6 4 7 | 8 3 5 | 9 2 1 |\n" +
            "-------------------------";
    private static final String SECOND_SOLUTION = "-------------------------\n" +
            "| 1 5 3 | 9 2 8 | 7 4 6 |\n" +
            "| 4 6 8 | 1 5 7 | 2 3 9 |\n" +
            "| 2 7 9 | 4 6 3 | 1 5 8 |\n" +
            "-------------------------\n" +
            "| 3 1 6 | 2 7 4 | 8 9 5 |\n" +
            "| 8 2 4 | 5 1 9 | 3 6 7 |\n" +
            "| 7 9 5 | 3 8 6 | 4 1 2 |\n" +
            "-------------------------\n" +
            "| 5 3 1 | 7 9 2 | 6 8 4 |\n" +
            "| 9 8 7 | 6 4 1 | 5 2 3 |\n" +
            "| 6 4 2 | 8 3 5 | 9 7 1 |\n" +
            "-------------------------";

    @Test
    void solveSudoku() {
        Collection<String> solutions =  SudokuRunner.run(FILE_PATH);
        Iterator<String> iterator = solutions.iterator();
        assertThat(iterator.next(), is(equalTo(FIRST_SOLUTION)));
        assertThat(iterator.next(), is(equalTo(SECOND_SOLUTION)));
    }
}